# Hydro Bundle for Development

Bundles Admin and Bucket for Flood\Hydro project.

## Quick Start

### React Admin

```bash
cd admin
npm i

npm run start
# localhost:9220
# or like defined in package.json
```

### PHP Framework, CMS

Bucket:

```bash
cd bucket
composer install

# Windows, using WSL
npm run start-win

# Unixoid
npm run start-uni

# localhost:9221
# or like defined in dev_server.conf
```

## Docker

Don't forget to modify ports in `docker-compose.yml` for your used ports in `bucket/dev_server.conf`.

It is recommended to add another container for each port, speeds up requests through not being limited by single-threaded PHP Dev-Server.

```bash
docker-compose up
docker-compose up --build

# bucket:
# http://localhost:9221
# or like defined in bucket/dev_server.conf AND docker-compose.yml

# mailhog email testing
# http://localhost:8025

# access e.g. MySQL from host with db server `host.docker.internal`
```